﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="controls" Type="Folder">
			<Item Name="mainCluster.ctl" Type="VI" URL="../controls/mainCluster.ctl"/>
			<Item Name="states.ctl" Type="VI" URL="../controls/states.ctl"/>
		</Item>
		<Item Name="Externals" Type="Folder">
			<Item Name="Instrument Drivers" Type="Folder">
				<Item Name="Instrument" Type="Folder">
					<Item Name="Instrument.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Instrument/Instrument.lvclass"/>
				</Item>
				<Item Name="Power Supply" Type="Folder">
					<Item Name="Agilent 661x.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent 661x/Agilent 661x.lvclass"/>
					<Item Name="Agilent 662x.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent 662x/Agilent 662x.lvclass"/>
					<Item Name="Agilent 663x.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent 663x/Agilent 663x.lvclass"/>
					<Item Name="Agilent 664x.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent 664x/Agilent 664x.lvclass"/>
					<Item Name="Agilent 665x.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent 665x/Agilent 665x.lvclass"/>
					<Item Name="Agilent 6032A.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent 6032A/Agilent 6032A.lvclass"/>
					<Item Name="Agilent 6674A.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent 6674A/Agilent 6674A.lvclass"/>
					<Item Name="Agilent E363xA.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent E363xA/Agilent E363xA.lvclass"/>
					<Item Name="Agilent E364xA.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent E364xA/Agilent E364xA.lvclass"/>
					<Item Name="Agilent E3631A.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent E3631A/Agilent E3631A.lvclass"/>
					<Item Name="Agilent E3646-49A.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent E3646-49A/Agilent E3646-49A.lvclass"/>
					<Item Name="Agilent E5052x (Ctrl).lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent E5052x (Ctrl)/Agilent E5052x (Ctrl).lvclass"/>
					<Item Name="Agilent E5052x (Pwr).lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent E5052x (Pwr)/Agilent E5052x (Pwr).lvclass"/>
					<Item Name="Agilent N57xxA.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent N57xxA/Agilent N57xxA.lvclass"/>
					<Item Name="Agilent N670xx.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent N670xx/Agilent N670xx.lvclass"/>
					<Item Name="Agilent N8737A.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Agilent N8737A/Agilent N8737A.lvclass"/>
					<Item Name="Keithley 24xx.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Keithley 24xx/Keithley 24xx.lvclass"/>
					<Item Name="Keithley 2200-20-5.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Keithley 2200-20-5/Keithley 2200-20-5.lvclass"/>
					<Item Name="Keithley 2280.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Keithley 2280/Keithley 2280.lvclass"/>
					<Item Name="Keithley 2304A.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Keithley 2304A/Keithley 2304A.lvclass"/>
					<Item Name="Kepco KLP 75-33.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Kepco KLP 75-33/Kepco KLP 75-33.lvclass"/>
					<Item Name="Keysight N69xxA.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Keysight N69xxA/Keysight N69xxA.lvclass"/>
					<Item Name="Power Supply.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Power Supply Base/Power Supply.lvclass"/>
					<Item Name="Rohde HMP.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Rohde HMP/Rohde HMP.lvclass"/>
					<Item Name="Simulated Power Supply.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Simulated Power Supply/Simulated Power Supply.lvclass"/>
					<Item Name="Xantrex Sorensen XFR.lvclass" Type="LVClass" URL="../Externals/Instrument Drivers/Power Supply/Xantrex Sorensen XFR/Xantrex Sorensen XFR.lvclass"/>
				</Item>
			</Item>
			<Item Name="Qorvo Common" Type="Folder">
				<Item Name="AQ Character Lineator" Type="Folder">
					<Item Name="Binary Deserializer" Type="Folder">
						<Item Name="Binary Deserializer.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator/Binary Deserializer/Binary Deserializer.lvclass"/>
					</Item>
					<Item Name="Binary Serializer" Type="Folder">
						<Item Name="Binary Serializer.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator/Binary Serializer/Binary Serializer.lvclass"/>
					</Item>
					<Item Name="Deserializer" Type="Folder">
						<Item Name="Deserializer.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator/Deserializer/Deserializer.lvclass"/>
					</Item>
					<Item Name="Formatter" Type="Folder">
						<Item Name="Advanced.mnu" Type="Document" URL="../Externals/Qorvo Common/AQ Character Lineator/Formatter/Advanced.mnu"/>
						<Item Name="Formatter.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator/Formatter/Formatter.lvclass"/>
						<Item Name="Include.vi" Type="VI" URL="../Externals/Qorvo Common/AQ Character Lineator/Formatter/Include.vi"/>
					</Item>
					<Item Name="JSON Deserializer" Type="Folder">
						<Item Name="JSON Deserializer.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator/JSON Deserializer/JSON Deserializer.lvclass"/>
						<Item Name="JSON Deserializer.vi" Type="VI" URL="../Externals/Qorvo Common/AQ Character Lineator/JSON Deserializer/JSON Deserializer.vi"/>
					</Item>
					<Item Name="JSON Serializer" Type="Folder">
						<Item Name="JSON Serializer.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator/JSON Serializer/JSON Serializer.lvclass"/>
						<Item Name="JSON Serializer.vi" Type="VI" URL="../Externals/Qorvo Common/AQ Character Lineator/JSON Serializer/JSON Serializer.vi"/>
					</Item>
					<Item Name="Serializable" Type="Folder">
						<Item Name="Serializable.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator/Serializable/Serializable.lvclass"/>
						<Item Name="serialize.names" Type="Document" URL="../Externals/Qorvo Common/AQ Character Lineator/Serializable/serialize.names"/>
					</Item>
					<Item Name="Serialization Common" Type="Folder">
						<Item Name="Serialization Common.lvlib" Type="Library" URL="../Externals/Qorvo Common/AQ Character Lineator/Serialization Common/Serialization Common.lvlib"/>
					</Item>
					<Item Name="Serializer" Type="Folder">
						<Item Name="Serializer.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator/Serializer/Serializer.lvclass"/>
					</Item>
					<Item Name="AQ Character Lineator.aliases" Type="Document" URL="../Externals/Qorvo Common/AQ Character Lineator/AQ Character Lineator.aliases"/>
					<Item Name="AQ Character Lineator.lvlps" Type="Document" URL="../Externals/Qorvo Common/AQ Character Lineator/AQ Character Lineator.lvlps"/>
					<Item Name="AQ Character Lineator.lvproj" Type="Document" URL="../Externals/Qorvo Common/AQ Character Lineator/AQ Character Lineator.lvproj"/>
					<Item Name="License.txt" Type="Document" URL="../Externals/Qorvo Common/AQ Character Lineator/License.txt"/>
					<Item Name="Time Stamp To U64 Date-Time Record.vi" Type="VI" URL="../Externals/Qorvo Common/AQ Character Lineator/Time Stamp To U64 Date-Time Record.vi"/>
					<Item Name="U64 Date-Time Record To Time Stamp.vi" Type="VI" URL="../Externals/Qorvo Common/AQ Character Lineator/U64 Date-Time Record To Time Stamp.vi"/>
					<Item Name="U64 Date-Time Record.ctl" Type="VI" URL="../Externals/Qorvo Common/AQ Character Lineator/U64 Date-Time Record.ctl"/>
				</Item>
				<Item Name="AQ Character Lineator Extensions" Type="Folder">
					<Item Name="INI Deserializer" Type="Folder">
						<Item Name="INI Deserializer.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator Extensions/INI Deserializer/INI Deserializer.lvclass"/>
					</Item>
					<Item Name="INI Serializer" Type="Folder">
						<Item Name="INI Serializer.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator Extensions/INI Serializer/INI Serializer.lvclass"/>
					</Item>
					<Item Name="MPTS Serialize Formatter" Type="Folder">
						<Item Name="MPTS Serialize Formatter.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator Extensions/MPTS Serialize Formatter/MPTS Serialize Formatter.lvclass"/>
					</Item>
					<Item Name="Serializable Simple Cluster" Type="Folder">
						<Item Name="Serializable Simple Cluster.lvclass" Type="LVClass" URL="../Externals/Qorvo Common/AQ Character Lineator Extensions/Serializable Simple Cluster/Serializable Simple Cluster.lvclass"/>
					</Item>
				</Item>
			</Item>
		</Item>
		<Item Name="subVIs" Type="Folder">
			<Item Name="CheckConnection.vi" Type="VI" URL="../subVIs/CheckConnection.vi"/>
		</Item>
		<Item Name="PS_ThumbWheel.vi" Type="VI" URL="../PS_ThumbWheel.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="Array of VData to VArray__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VArray__ogtk.vi"/>
				<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
				<Item Name="Array Size(s)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array Size(s)__ogtk.vi"/>
				<Item Name="Array to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array to Array of VData__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Cluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Cluster to Array of VData__ogtk.vi"/>
				<Item Name="Encode Section and Key Names__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/variantconfig/variantconfig.llb/Encode Section and Key Names__ogtk.vi"/>
				<Item Name="Format Variant Into String__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Format Variant Into String__ogtk.vi"/>
				<Item Name="Get Array Element TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Array Element TD__ogtk.vi"/>
				<Item Name="Get Array Element TDEnum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Array Element TDEnum__ogtk.vi"/>
				<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
				<Item Name="Get Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name__ogtk.vi"/>
				<Item Name="Get Default Data from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Default Data from TD__ogtk.vi"/>
				<Item Name="Get Element TD from Array TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Element TD from Array TD__ogtk.vi"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get Refnum Type Enum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Refnum Type Enum from Data__ogtk.vi"/>
				<Item Name="Get Refnum Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Refnum Type Enum from TD__ogtk.vi"/>
				<Item Name="Get Strings from Enum TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum TD__ogtk.vi"/>
				<Item Name="Get Strings from Enum__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Strings from Enum__ogtk.vi"/>
				<Item Name="Get TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get TDEnum from Data__ogtk.vi"/>
				<Item Name="Get Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Variant Attributes__ogtk.vi"/>
				<Item Name="Get Waveform Type Enum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Waveform Type Enum from Data__ogtk.vi"/>
				<Item Name="Get Waveform Type Enum from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Waveform Type Enum from TD__ogtk.vi"/>
				<Item Name="LVOOP Get Default Object__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/LVOOP Get Default Object__ogtk.vi"/>
				<Item Name="LVOOP Return Class Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/LVOOP Return Class Name__ogtk.vi"/>
				<Item Name="MGI Clear Error.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Clear Error.vi"/>
				<Item Name="MGI Wait (Double).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Timing/MGI Wait/MGI Wait (Double).vi"/>
				<Item Name="MGI Wait (U32).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Timing/MGI Wait/MGI Wait (U32).vi"/>
				<Item Name="MGI Wait.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Timing/MGI Wait.vi"/>
				<Item Name="Parse String with TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Parse String with TDs__ogtk.vi"/>
				<Item Name="Refnum Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Refnum Subtype Enum__ogtk.ctl"/>
				<Item Name="Reshape Array to 1D VArray__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Reshape Array to 1D VArray__ogtk.vi"/>
				<Item Name="Resolve Timestamp Format__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Resolve Timestamp Format__ogtk.vi"/>
				<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
				<Item Name="Set Enum String Value__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Enum String Value__ogtk.vi"/>
				<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
				<Item Name="Strip Units__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Strip Units__ogtk.vi"/>
				<Item Name="Trim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String Array)__ogtk.vi"/>
				<Item Name="Trim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String)__ogtk.vi"/>
				<Item Name="Trim Whitespace__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace__ogtk.vi"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				<Item Name="Waveform Subtype Enum__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Waveform Subtype Enum__ogtk.ctl"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Class Retrieval.lvlib" Type="Library" URL="/&lt;vilib&gt;/NI/Class Retrieval/Name to Path Conversion/Class Retrieval.lvlib"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Filter.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/NI/AQ Character Lineator/Filter/Filter/Filter.lvclass"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Qualified Name Array To Single String.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VISA GPIB Control REN Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA GPIB Control REN Mode.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="Adjust for Cable Resistance.vi" Type="VI" URL="../Externals/Instrument Drivers/common/Adjust for Cable Resistance.vi"/>
			<Item Name="Close VISA Session.vi" Type="VI" URL="../Externals/Instrument Drivers/common/Close VISA Session.vi"/>
			<Item Name="E5052x Config Freq and Power Sweep.vi" Type="VI" URL="../Externals/Instrument Drivers/Power Supply/Common/E5052x Config Freq and Power Sweep.vi"/>
			<Item Name="E5052x Config Trigger.vi" Type="VI" URL="../Externals/Instrument Drivers/Power Supply/Common/E5052x Config Trigger.vi"/>
			<Item Name="E5052x Read Tester.vi" Type="VI" URL="../Externals/Instrument Drivers/Power Supply/Common/E5052x Read Tester.vi"/>
			<Item Name="E5052x Wait For Acquistion.vi" Type="VI" URL="../Externals/Instrument Drivers/Power Supply/Common/E5052x Wait For Acquistion.vi"/>
			<Item Name="Open VISA Session.vi" Type="VI" URL="../Externals/Instrument Drivers/common/Open VISA Session.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
