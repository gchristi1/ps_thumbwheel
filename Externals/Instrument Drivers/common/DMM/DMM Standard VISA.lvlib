﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="16008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6982244</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.FriendGUID" Type="Str">17545eaf-f462-4108-9a4b-08e90f7f3a44</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;A#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)_!!!*Q(C=\&gt;7^41."%)&lt;B$U2![A[121=D/H!,&lt;G%[1![=%,K&amp;E3PAE#Q2OY6*(&lt;I%X-,RXD+S,U!Y!931\\TWX&lt;&gt;\MQ`XB^377_F'`&lt;FF_?83^^*SS9@W]&gt;UWBJ_W3@_Q/`J5@RV3`;0V^-0I9`_I`H)UI4[:PWKI[J`THVFU73\,`V[?TDY%0`Y=8!V0?E^,7N#=:OXJ\J-]S:-]S:-]S9-]S)-]S)-]S*X=S:X=S:X=S9X=S)X=S)X=S.N+,H+2CRR3-HES56)U+:!-BK,E+`%EHM34?.B6YEE]C3@R*"['+0%EHM34?")0BSHR**\%EXA3$[7;*.N+DC@R5&amp;["*`!%HM!4?*B3A3=!"*-&amp;B9-C-"2U"BO"*`!%(D96?!*0Y!E]A9&gt;O":\!%XA#4_$BE(:7IGG'F2Q0:?2Y()`D=4S/B^*S0)\(]4A?R].U=DS/RU%Y%TL&amp;)=AZS"HA\$A?R]/0()`D=4S/R`(1V;[1NT-T;);6()`B-4S'R`!9(EL)]"A?QW.Y$!^F:8A-D_%R0);(K72Y$)`B-3$'J%QPIZBRI$()#!Q0HX;X7,N+U3476KFO8N6.K&lt;L:6$?2[O:18846R62&gt;*.8*6ZV5V=F3H146([&gt;#KT#K361($Q.VY(N0W^)[WIKWI-VJ-^K5.BE/`?;"B]."_`V?W_V78&gt;&gt;JN6JJM6BI0J^L.JNJ/JVK-JE=8Q.XL-=8QOG^N.\MVM`XO]?OW\W_&lt;.ZW[]X&lt;QS`^@`Y$\U:&gt;;TQ(Z_A&gt;GKJ&gt;B!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">369131520</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Friends List" Type="Friends List">
		<Item Name="DMM.lvclass" Type="Friended Library" URL="../../../DMMs/DMM/DMM.lvclass"/>
		<Item Name="Agilent 344xxA.lvclass" Type="Friended Library" URL="../../../DMMs/Agilent 344xxA/Agilent 344xxA.lvclass"/>
		<Item Name="Fluke 45.lvclass" Type="Friended Library" URL="../../../DMMs/Fluke 45/Fluke 45.lvclass"/>
		<Item Name="Fluke 8845A.lvclass" Type="Friended Library" URL="../../../DMMs/Fluke 8845A/Fluke 8845A.lvclass"/>
		<Item Name="HP 3478A.lvclass" Type="Friended Library" URL="../../../DMMs/HP 3478A/HP 3478A.lvclass"/>
	</Item>
	<Item Name="Community" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">4</Property>
		<Item Name="Configure Measurement.vi" Type="VI" URL="../Configure Measurement.vi"/>
		<Item Name="Fetch Measurement.vi" Type="VI" URL="../Fetch Measurement.vi"/>
		<Item Name="Get Function.vi" Type="VI" URL="../Get Function.vi"/>
		<Item Name="Get Trigger Source.vi" Type="VI" URL="../Get Trigger Source.vi"/>
		<Item Name="IDN Query.vi" Type="VI" URL="../IDN Query.vi"/>
		<Item Name="Initiate.vi" Type="VI" URL="../Initiate.vi"/>
		<Item Name="Reset.vi" Type="VI" URL="../Reset.vi"/>
		<Item Name="Set Function.vi" Type="VI" URL="../Set Function.vi"/>
		<Item Name="Set Trigger Source.vi" Type="VI" URL="../Set Trigger Source.vi"/>
	</Item>
</Library>
