﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="16008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Ancestors" Type="Folder">
			<Item Name="Instrument.lvclass" Type="LVClass" URL="../../Instrument/Instrument.lvclass"/>
		</Item>
		<Item Name="base" Type="Folder"/>
		<Item Name="Common" Type="Folder">
			<Item Name="Instrument Common" Type="Folder">
				<Item Name="Adjust for Cable Resistance.vi" Type="VI" URL="../../common/Adjust for Cable Resistance.vi"/>
				<Item Name="Close VISA Session.vi" Type="VI" URL="../../common/Close VISA Session.vi"/>
				<Item Name="Open VISA Session.vi" Type="VI" URL="../../common/Open VISA Session.vi"/>
			</Item>
			<Item Name="Power Supply Common" Type="Folder">
				<Item Name="E5052x Config Freq and Power Sweep.vi" Type="VI" URL="../Common/E5052x Config Freq and Power Sweep.vi"/>
				<Item Name="E5052x Config Trigger.vi" Type="VI" URL="../Common/E5052x Config Trigger.vi"/>
				<Item Name="E5052x Read Tester.vi" Type="VI" URL="../Common/E5052x Read Tester.vi"/>
				<Item Name="E5052x Wait For Acquistion.vi" Type="VI" URL="../Common/E5052x Wait For Acquistion.vi"/>
			</Item>
		</Item>
		<Item Name="Implementations" Type="Folder">
			<Item Name="Plugins" Type="Folder">
				<Item Name="Agilent 661x.lvclass" Type="LVClass" URL="../Agilent 661x/Agilent 661x.lvclass"/>
				<Item Name="Agilent 662x.lvclass" Type="LVClass" URL="../Agilent 662x/Agilent 662x.lvclass"/>
				<Item Name="Agilent 663x.lvclass" Type="LVClass" URL="../Agilent 663x/Agilent 663x.lvclass"/>
				<Item Name="Agilent 664x.lvclass" Type="LVClass" URL="../Agilent 664x/Agilent 664x.lvclass"/>
				<Item Name="Agilent 665x.lvclass" Type="LVClass" URL="../Agilent 665x/Agilent 665x.lvclass"/>
				<Item Name="Agilent 6032A.lvclass" Type="LVClass" URL="../Agilent 6032A/Agilent 6032A.lvclass"/>
				<Item Name="Agilent 6674A.lvclass" Type="LVClass" URL="../Agilent 6674A/Agilent 6674A.lvclass"/>
				<Item Name="Agilent E363xA.lvclass" Type="LVClass" URL="../Agilent E363xA/Agilent E363xA.lvclass"/>
				<Item Name="Agilent E364xA.lvclass" Type="LVClass" URL="../Agilent E364xA/Agilent E364xA.lvclass"/>
				<Item Name="Agilent E3631A.lvclass" Type="LVClass" URL="../Agilent E3631A/Agilent E3631A.lvclass"/>
				<Item Name="Agilent E3646-49A.lvclass" Type="LVClass" URL="../Agilent E3646-49A/Agilent E3646-49A.lvclass"/>
				<Item Name="Agilent E5052x (Ctrl).lvclass" Type="LVClass" URL="../Agilent E5052x (Ctrl)/Agilent E5052x (Ctrl).lvclass"/>
				<Item Name="Agilent E5052x (Pwr).lvclass" Type="LVClass" URL="../Agilent E5052x (Pwr)/Agilent E5052x (Pwr).lvclass"/>
				<Item Name="Agilent N57xxA.lvclass" Type="LVClass" URL="../Agilent N57xxA/Agilent N57xxA.lvclass"/>
				<Item Name="Agilent N670xx.lvclass" Type="LVClass" URL="../Agilent N670xx/Agilent N670xx.lvclass"/>
				<Item Name="Agilent N8737A.lvclass" Type="LVClass" URL="../Agilent N8737A/Agilent N8737A.lvclass"/>
				<Item Name="Keithley 24xx.lvclass" Type="LVClass" URL="../Keithley 24xx/Keithley 24xx.lvclass"/>
				<Item Name="Keithley 2200-20-5.lvclass" Type="LVClass" URL="../Keithley 2200-20-5/Keithley 2200-20-5.lvclass"/>
				<Item Name="Keithley 2304A.lvclass" Type="LVClass" URL="../Keithley 2304A/Keithley 2304A.lvclass"/>
				<Item Name="Kepco KLP 75-33.lvclass" Type="LVClass" URL="../Kepco KLP 75-33/Kepco KLP 75-33.lvclass"/>
				<Item Name="NI Analog Output.lvclass" Type="LVClass" URL="../NI Analog Output/NI Analog Output.lvclass"/>
				<Item Name="NI SMU (High Z).lvclass" Type="LVClass" URL="../NI SMU (High Z)/NI SMU (High Z).lvclass"/>
				<Item Name="NI SMU.lvclass" Type="LVClass" URL="../NI SMU/NI SMU.lvclass"/>
				<Item Name="Simulated Power Supply.lvclass" Type="LVClass" URL="../Simulated Power Supply/Simulated Power Supply.lvclass"/>
				<Item Name="Xantrex Sorensen XFR.lvclass" Type="LVClass" URL="../Xantrex Sorensen XFR/Xantrex Sorensen XFR.lvclass"/>
			</Item>
			<Item Name="Power Supply.lvclass" Type="LVClass" URL="../Power Supply Base/Power Supply.lvclass"/>
		</Item>
		<Item Name="Test Panel.vi" Type="VI" URL="../Test Panel.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="niDCPower Abort.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Abort.vi"/>
				<Item Name="niDCPower Close.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Close.vi"/>
				<Item Name="niDCPower Commit.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Commit.vi"/>
				<Item Name="niDCPower Configure Current Level Range.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Current Level Range.vi"/>
				<Item Name="niDCPower Configure Current Level.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Current Level.vi"/>
				<Item Name="niDCPower Configure Current Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Current Limit.vi"/>
				<Item Name="niDCPower Configure Output Function.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Output Function.vi"/>
				<Item Name="niDCPower Configure Voltage Level Range.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Voltage Level Range.vi"/>
				<Item Name="niDCPower Configure Voltage Level.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Voltage Level.vi"/>
				<Item Name="niDCPower Configure Voltage Limit.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Configure Voltage Limit.vi"/>
				<Item Name="niDCPower Current Limit Behavior.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Current Limit Behavior.ctl"/>
				<Item Name="niDCPower Initialize With Channels.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Initialize With Channels.vi"/>
				<Item Name="niDCPower Initiate.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Initiate.vi"/>
				<Item Name="niDCPower IVI Error Converter.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower IVI Error Converter.vi"/>
				<Item Name="niDCPower Measure Multiple.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Measure Multiple.vi"/>
				<Item Name="niDCPower Output Function.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Output Function.ctl"/>
				<Item Name="niDCPower Output State.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Output State.ctl"/>
				<Item Name="niDCPower Query In Compliance.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Query In Compliance.vi"/>
				<Item Name="niDCPower Query Output State.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Query Output State.vi"/>
				<Item Name="niDCPower Reset.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Reset.vi"/>
				<Item Name="niDCPower Wait For Event - Event.ctl" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Wait For Event - Event.ctl"/>
				<Item Name="niDCPower Wait For Event.vi" Type="VI" URL="/&lt;instrlib&gt;/niDCPower/nidcpower.llb/niDCPower Wait For Event.vi"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="MGI Clear Error.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Clear Error.vi"/>
				<Item Name="MGI Suppress Error Code (Array).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code/MGI Suppress Error Code (Array).vi"/>
				<Item Name="MGI Suppress Error Code (Scalar).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code/MGI Suppress Error Code (Scalar).vi"/>
				<Item Name="MGI Suppress Error Code.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code.vi"/>
				<Item Name="MGI Wait (Double).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Timing/MGI Wait/MGI Wait (Double).vi"/>
				<Item Name="MGI Wait (U32).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Timing/MGI Wait/MGI Wait (U32).vi"/>
				<Item Name="MGI Wait.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Timing/MGI Wait.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="DAQmx Stop Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Stop Task.vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VISA GPIB Control REN Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA GPIB Control REN Mode.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="nidcpower_32.dll" Type="Document" URL="nidcpower_32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Power Supply Plugins" Type="Source Distribution">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{8F5789CA-86D8-4A51-926A-4C812479A973}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Power Supply Plugins</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/builds/Instrument Drivers</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{94AAB4BB-98E8-43F5-9FD0-087BA3F08539}</Property>
				<Property Name="Bld_version.build" Type="Int">1</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[0].path" Type="Path">/C/builds/Instrument Drivers</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/builds/Instrument Drivers/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{820B58CC-3885-4A06-A697-FDC1D2D64156}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Ancestors</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Exclude</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Implementations</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
			</Item>
		</Item>
	</Item>
</Project>
